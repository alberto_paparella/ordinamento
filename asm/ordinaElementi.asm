.data
a:	.word	0x04, 0x01, 0x08, 0x00, 0x05
n:	.word	0x05

.text
main:
	la	$s0, a
	lw	$s1, n
	
	add	$t0, $zero, $zero	# dl
loop_est:
	add	$t1, $t0, $zero		#j = dl
	
	sll	$t2, $t0, 2		# dl *= 4
	add	$t2, $t2, $s0		# calcolo indirizzo a[dl]
	lw	$t2, 0($t2)		# carico a[dl], $t2 e' m
	
	bne	$t1, $zero, ent_cond	# if j == 0, skip loop_int
	j	exit_loop_int
	
ent_cond:
	addi	$t3, $t1, -1		# j - 1
	sll	$t3, $t3, 2		# (j - 1) *= 4
	add	$t3, $t3, $s0		# calcolo l'indirizzo di a[j - 1]
	lw	$t3, 0($t3)		# carico a[j - 1]
	slt	$t4, $t2, $t3		# if !(m < a[j - 1]) skip loop_int
	bne	$t4, $zero, loop_int
	j	exit_loop_int
	
loop_int:
	sll	$t4, $t1, 2		# j *= 4 in $t4 che sovrascrivo
	add	$t4, $t4, $s0		# calcolo indirizzo a[j]
	sw	$t3, 0($t4)		# salvo a[j - 1] in a [j]
	
	addi	$t1, $t1, -1		# j--
	
	bne	$t1, $zero, exit_cond	#if j == 0, exit loop_int
	j	exit_loop_int

exit_cond:
	addi	$t3, $t1, -1		# j - 1
	sll	$t3, $t3, 2		# (j - 1) *= 4
	add	$t3, $t3, $s0		# calcolo l'indirizzo di a[j - 1]
	lw	$t3, 0($t3)		# carico a[j - 1]
	slt	$t5, $t2, $t3		# if m < a[j - 1] enter loop_int
	beq	$t5, $zero, exit_loop_int
	j	loop_int
	
exit_loop_int:
	sll	$t4, $t1, 2		# j *= 4 in $t4 che sovrascrivo
	add	$t4, $t4, $s0		# calcolo indirizzo a[j]
	sw	$t2, 0($t4)		# salvo m in a[j]
	
	addi	$t0, $t0, 1		# dl++
	beq	$t0, $s1, exit_loop_est	# if dl == s1, exit loop_est
	j	loop_est
	
exit_loop_est:
	add	$t0, $zero, $zero	# i = 0
	
stampa:
	sll	$t1, $t0, 2		# i *= 4
	add	$t1, $t1, $s0		# calcolo indirizzo a[i]
	lw	$t1, 0($t1)		# carico a[i] in $t1
	
	Addi $a0, $t1, 0
	Addi $v0, $zero, 1
	syscall				# print a[i]
	
	addi	$t0, $t0, 1		# i++
	beq	$t0, $s1, end		# if i == n skip to end
	j	stampa
	
end:
	li	$v0, 10
	syscall				# fine